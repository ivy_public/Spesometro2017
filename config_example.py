OPENERP = {
    'username'         : 'username' ,    # user
    'pwd'              : 'password' ,    # password user
    'dbname'           : 'database' ,    # db
    'address_common'   : 'http://localhost:port/xmlrpc/common',
    'address_object'   : 'https://localhost:port/xmlrpc/object'
}

# ID DATABASE ALIQUOTE DA ESCLUDERE
TAXEXCLUDE = []


class valoridefault():
    # SEZIONALI
    # COMPILARE IL DICT CON:
    #     IDSEZIONALE : { TYCOM:[0 se doc uscita|1 se doc ingresso], TYDOC: VEDITABELLA}
    # X TYDOC:
    """
    TD01 = Fattura
    TD04 = Nota di credito
    TD05 = Nota di debito
    TD07 = Fattura semplificata
    TD08 = Nota di credito semplificata
    TD10 = Fattura per acquisto intracomunitario beni
    TD11 = Fattura per acquisto intracomunitario servizi"
    """    
    TP = {
        '1':    {'tycom':0,'tydoc':'TD01'},      #sez vendite
        '14':   {'tycom':0,'tydoc':'TD01'},      #sez vendite PA
        '15':   {'tycom':0,'tydoc':'TD01'},      #sez autoinvoice
        '3':    {'tycom':0,'tydoc':'TD04'},      #sez note di credito clienti
        '2':    {'tycom':1,'tydoc':'TD01'},      #sez acquisti
        '4':    {'tycom':1,'tydoc':'TD04'},      #sez note di credito fornitori
    }

    # journalid per esigibilita iva S
    idesigibile = 14

    # NATURA:
    """
    N1 = Escluso ex art.15
    N2 = Non soggetto
    N3 = Non imponibile
    N4 = Esente
    N5 = Regime del margine / IVA non esposta in fattura
    N6 = Inversione contabile
    N7 = IVA assolta in altro stato UE
    """

    # id partner N1 con aliquota a 0 (esempio anticipo spese di un commercialista) N1
    clientiN1 = []

    # id partner N2 es. spese condominiali
    clientiN2 = []

    # id partner N3 esenti per articolo 8
    clientiN3 = []

    # id partner speciali N4
    clientiN4 = []

    # id partner N5
    clientiN5 = []

    # id partner N6
    clientiN6 = []

    # id partner N7
    clientiN7 = []
