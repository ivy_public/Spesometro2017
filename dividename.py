import string
from lib.codicefiscale.codicefiscale import cf
from dateutil import parser

def div_check(cf,code,pattern):
    bday = cf.get_birthday()
    sex = cf.get_sex()

    bday = parser.parse(bday)

    ncode = cf.build(pattern,'',bday,sex,code[-5:-1])

    if ncode[:3] == code[:3]:
        return True
    else:
        return False

def div_recursion(cf,code,surname,part,index=[],counter=0,debug=False):

    if debug:
        print('---------')
        print('--start--')
        print(surname)
        print(part)

    # USCITA = NON CI SONO PIU' PARTI DA PROVARE    
    if len(part) == 0:
        return False

    index.append(counter)

    surname = surname + part[0]

    part.pop(0)

    if debug:
        print("--")
        print(surname)
        print(part)

    result = div_check(cf,code,surname)

    if debug:
        print("--")
        print(result)

    if result:
        name = " ".join(part)
        return [surname,name,index]
    else:
        result = div_recursion(cf,code,surname,part,index,counter+1,debug)
        if debug:
            print('-back-')
            print(result)
        return result

def dividename(code,nome,debug=False):

    try:

        nomet = nome.translate(string.maketrans("",""), string.punctuation)
        name = nomet.split(' ')
        cfo = cf(code)

        if debug:
            print('######### 1 #########')
            print(code)
            print(name)

        result = div_recursion(cfo,code,'',name,[],0,debug)

        if debug:
            print('First call result')
            print(result)

        if result:
            part = nome.split(' ')
            cog = [part[i] for i in result[2]]
            nom = [item for item in part if item not in cog]
            cog = ' '.join(cog)
            nom = ' '.join(nom)
            return [cog,nom]
        else:
            name = list(reversed(nomet.split(' ')))
            if debug:
                print('######### 2 #########')
                print(code)
                print(name)

            result = div_recursion(cfo,code,'',name,[],0,debug)
            if debug:
                print('First call result')
                print(result)

            if result:
                part = nome.split(' ')
                part = list(reversed(part))
                cog = [part[i] for i in result[2]]
                nom = [item for item in part if item not in cog]
                cog = ' '.join(cog)
                nom = ' '.join(nom)
                return [cog,nom]

        if not result:
            nome1 = nome.split(' ',1)
            result = dividename(code,nome1[1],debug)
            if result:
                return [nome1[1],nome1[0]]
            else:
                nome1 = list(reversed(nome.split(' ')))
                nome1 = ' '.join(nome1)
                nome1 = nome1.split(' ',1)
                result = dividename(code,nome1[1],debug)
                if result:
                    return [nome1[1],nome1[0]]

        return False
    except:
        return False

"""
code='CODICEFISCALE'
name="CAMPONAME ERP"

# CHIAMATA: CF,NOME,DEBUG
result = dividename(code,name,True)
print(result)
"""