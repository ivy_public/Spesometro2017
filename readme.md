comunicazione dati fatture emesse e ricevute

 - Author: Sandro<sandro@ivynet.it>
 - Date: 2017-09-19
 - Version: 1.0
 - Openerp: 7.0

Script Python per l'estrazione dei dati delle fatture emesse e ricevute e generazione file excel conforme all'import su datev

 - copiare il file config_example.py in config.py
 - compilare correttamente le informazioni di accesso al proprio openerp e i valori di default come indicato nelle note
 - eseguire:
     # python spesometro.py

 - vengono prodotti 3 file:
   - result.csv (file contenente l'export corretto
   - error.csv (file contenente le righe delle fatture con errori di generazione
   - taxerror.csv (file contenente le righe delle fatture con problemi nelle aliquote

NOTE:
 - La Natura è gestita con l'ID di database del partner, basta modificarlo nel file config
 - Si possono escludere delle aliquote dal file config
 - Vanno configurati i sezionali nel file config
 - Lo script risolve anche il problema del nome e cognome, ossia prova a suddividere, basandosi sul codice fiscale, il campo name di openerp in cognome e nome, non riconosce alcuni casi con lettere accentate o con doppio o triplo cognome 
 - Il modello in excel contiene la descrizione completa di tutti i campi che devono essere generati

ATTENZIONE:
 - Alcuni campi di database possono essere presenti solamente grazie ad altri moduli installati, es: province in res_partner, verificare che i campi utilizzati esistano nella propria istanza