
#!/usr/bin/python2.7

import sys
import os
import urllib, csv
import operator
from utils import *
from config import *
import erpconnect
from lib.XlsxWriter.workbook import Workbook
from dividename import dividename
import pprint

class ErpConnector(erpconnect.ErpConnector):

    countries = {
        False:{'code':'OO','id':0,'name':'NoCountry','inue':False}
    }
    districts = {
        False:{'code':'','id':0,'name':'NoDistrict'}
    }
    districts2 = {
        False:{'code':'','id':0,'name':'NoDistrict'}
    }    
    taxes = {
        False:{'name':'','description':'','amount':0}
    }

    def __init__(self,auth):
        super(self.__class__,self).__init__(auth)
        self.taxexclude = TAXEXCLUDE

    def getInvoiceIDS(self,start,end):
        ids = self.search('account.invoice',[('registration_date','>=',start),('registration_date','<=',end)])
        return ids

    def getInvoiceLineIDS(self,invids):
        ids = self.search('account.invoice.line',[('invoice_id','in',invids)])
        return ids

    def getInvoiceLineDATA(self,invlineids):
        fields = ['id','account_id','invoice_id','price_subtotal','partner_id','reverse_charge','invoice_line_tax_id','journal_id']
        invlinedata = self.read('account.invoice.line',invlineids,fields)

        data = []

        for item in invlinedata:
            fields = ['id','name','street','city','vat','fiscalcode','country_id','state_id','province','zip',]

            partner = self.read('res.partner',item['partner_id'][0],fields)

            if not partner['country_id'][0] in self.countries:
                fields = ['id','name','code','inue']
                country = self.read('res.country',partner['country_id'][0],fields)
                self.countries.update({partner['country_id'][0]:country})
            partner['country_id'] = self.countries[partner['country_id'][0]]
            
            if not partner['state_id'] and not partner['province']:
                partner['provincia'] = self.districts[False]
            elif partner['state_id']:
                if not partner['state_id'][0] in self.districts:
                    fields = ['id','name','code']
                    district = self.read('res.country.state',partner['state_id'][0],fields)
                    self.districts.update({partner['state_id'][0]:district})
                partner['provincia'] = self.districts[partner['state_id'][0]]
            else:
                if not partner['province'][0] in self.districts2:
                    fields = ['id','name','code']
                    district = self.read('res.province',partner['province'][0],fields)
                    self.districts2.update({partner['province'][0]:district})
                partner['provincia'] = self.districts2[partner['province'][0]]
            
            item['partner_id'] = partner


            fields = ['id','number','fiscal_position','date_invoice','registration_date','journal_id','period_id']
            invoice = self.read('account.invoice',item['invoice_id'][0],fields)

            taxes = []
            for tassa in item['invoice_line_tax_id']:
                if tassa not in self.taxexclude:
                    if tassa not in self.taxes:
                        taxes.append(tassa)
            tasse = self.read('account.tax',taxes,['id','name','description','amount'])
            for tassa in tasse:
                if tassa['id'] not in self.taxes:
                    self.taxes.update({tassa['id']:tassa})
            taxes = []
            for tassa in item['invoice_line_tax_id']:
                if tassa not in self.taxexclude:
                    taxes.append(self.taxes[tassa])

            item['taxes'] = taxes

            item['invoice_id'] = invoice

            data.append(item)

        return data

class spesometro(valoridefault):

    erp = False
    datestart = False
    dateend = False
    data = []
    generror = []
    generrorttax = []
    invlist = []
    invdict = {}

    def __init__(self,datestart='2017-01-01',dateend='2017-06-30'):
        self.erp =ErpConnector(OPENERP)
        self.datestart = datestart
        self.dateend = dateend
        self.popolate()
        self.pp = pprint.PrettyPrinter(indent=4)

    def popolate(self):
        invids = self.erp.getInvoiceIDS(self.datestart,self.dateend)
        invlineids = self.erp.getInvoiceLineIDS(invids)
        invlinedata = self.erp.getInvoiceLineDATA(invlineids)

        self.data = invlinedata

    def generaData(self):
        for item in self.data:
            ritem = self.parseInvoiceLine(item)

            if ritem:
                if ritem['error']:
                    self.generror.append(ritem)
                elif ritem['errortax']:
                    self.generrorttax.append(ritem)
                else:
                    if ritem['numero'] not in self.invlist:
                        self.invlist.append(ritem['numero'])
                        self.invdict.update({ritem['numero']:{ritem['aliquota']:ritem}})
                    else:
                        if ritem['aliquota'] not in self.invdict[ritem['numero']]:
                            self.invdict[ritem['numero']].update({ritem['aliquota']:ritem})
                        else:
                            tmp = self.invdict[ritem['numero']][ritem['aliquota']]  
                            tmp['imponibile'] = tmp['imponibile'] + ritem['imponibile']
                            tmp['imposta'] = tmp['imposta'] + ritem['imposta']
                            self.invdict[ritem['numero']][ritem['aliquota']] = tmp                
                

    def parseInvoiceLine(self,row):      
        rd = {
            'error':False,'errortax':False,
            'tycom':'','tydoc':'',
            'country':'',
            'vat':'','cf':'',
            'denom':'','cognome':'','nome':'',
            'indirizzo':'','civico':'','cap':'','comune':'','provincia':'','nazione':'',
            'numero':'',
            'data':'','rdata':'',
            'imponibile':0,'aliquota':0,'imposta':0,
            'natura':'','esigibile':'I'
        }

        # TIPO
        rd['tycom'] = self.TP[str(row['invoice_id']['journal_id'][0])]['tycom']
        rd['tydoc'] = self.TP[str(row['invoice_id']['journal_id'][0])]['tydoc']
        # COUNTRY CODE
        rd['country'] = row['partner_id']['country_id']['code']
        # PARTITA IVA
        if row['partner_id']['vat']:
            rd['vat'] = row['partner_id']['vat']
            if rd['vat'][:2]:
                rd['vat'] = rd['vat'][2:]
        
        # CODICE FISCALE
        if rd['vat'] == '' and row['partner_id']['fiscalcode']:
            rd['cf'] = row['partner_id']['fiscalcode'].upper()
            rd['country'] = ''
        elif rd['vat'] == '' and not row['partner_id']['fiscalcode']:
            rd['error'] = True

        if rd['vat'] != '':
            rd['denom'] = row['partner_id']['name']
        else:
            name = row['partner_id']['name']
            cf = rd['cf'].upper()

            result = dividename(cf,name)
            if result:
                rd['cognome'] = result[0]
                rd['nome'] = result[1]
            else:
                rd['cognome'] = row['partner_id']['name']
                rd['nome'] = 'ERROR'

        # SEDE
        if rd['country'] == 'IT':
            rd['indirizzo'] = row['partner_id']['street'] or ''
            rd['cap'] = row['partner_id']['zip'] or ''
            rd['comune'] = row['partner_id']['city'] or ''
            rd['provincia'] = row['partner_id']['provincia']['code'] or ''
            rd['nazione'] = row['partner_id']['country_id']['code'] or ''

        elif str(row['invoice_id']['journal_id'][0]) in ['2','4'] :
            # SE ESTERO ACQUISTO BENI
            rd['tydoc'] = 'TD10'

        # NUMERO FATTURA
        rd['numero'] = row['invoice_id']['number'].replace('/','')
        
        # DATA
        if rd['tycom'] == 0:
            rdata = row['invoice_id']['registration_date'].split('-')
            rd['data'] = rdata[2]+'/'+rdata[1]+'/'+rdata[0]
        else:
            rdata = row['invoice_id']['date_invoice'].split('-')
            rd['data'] = rdata[2]+'/'+rdata[1]+'/'+rdata[0]
            rdata = row['invoice_id']['registration_date'].split('-')
            rd['rdata'] = rdata[2]+'/'+rdata[1]+'/'+rdata[0]

        # IMPONIBILE
        rd['imponibile'] = float(row['price_subtotal'])

        # ALIQUOTA e TASSA
        if len(row['taxes']) != 1:
            rd['errortax'] = True
            rd['aliquota'] = ''
            for item in row['taxes']:
                rd['aliquota'] = rd['aliquota']  + ' ' + str(item['name'])
        else:
            rd['aliquota'] = int(float(row['taxes'][0]['amount']) * 100)
            rd['imposta'] = rd['imponibile']*float(row['taxes'][0]['amount'])

        # NATURA
        if rd['aliquota'] <= 0 or rd['aliquota'] > 100:
            rd['errortax'] = True

        # ESIG IVA
        if row['invoice_id']['journal_id'][0] == self.idesigibile:
            rd['esigibile'] = 'S'

        # TUTTO A 0
        if rd['imponibile'] == 0 and rd['aliquota'] == '':
            return False

        # N1
        if row['partner_id']['id'] in self.clientiN1 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N1'

        # N2
        if row['partner_id']['id'] in self.clientiN2 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N2'

        # N3
        if row['partner_id']['id'] in self.clientiN3 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N3'

        # N4
        if row['partner_id']['id'] in self.clientiN4 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N4'

        # N5
        if row['partner_id']['id'] in self.clientiN5 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N5'

        # N6
        if row['partner_id']['id'] in self.clientiN6 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N6'

        # N7
        if row['partner_id']['id'] in self.clientiN7 and rd['aliquota'] == 0:
            rd['errortax'] = False
            rd['natura'] = 'N7'

        return rd

    def getAllDBData(self,stdo=False):
        if stdo:
            self.pp.pprint(self.data)
        return self.data

    def getAllData(self,stdo=False):
        if stdo:
            self.pp.pprint(self.invdict)
        return self.invdict

    def getAllError(self,stdo=False):
        if stdo:
            self.pp.pprint(self.generror)
        return self.generror

    def getAllTaxError(self,stdo=False):
        if stdo:
            self.pp.pprint(self.generrorttax)
        return self.generrorttax


    def genCSV(self):
        self.genCSVData()
        self.genCSVError()
        self.genCSVTaxError()


    def genCSVData(self):
        toprint = []

        invnum = self.invlist
        invnum.sort()

        for num in invnum:
            invdata = self.invdict[num]
            for aliq,item in invdata.items():
                toprint.append(item)

        self.makeCSV(toprint,'result')

    def genCSVError(self):
        self.makeCSV(self.generror,'error')

    def genCSVTaxError(self):
        self.makeCSV(self.generrorttax,'taxerror')


    def makeCSV(self,data,filename):

        header = ['Tipo comunicazione','Nazione','P.IVA/Id.fisc.estero','Codice fiscale IT','Denominazione','Cognome','Nome','Sede - Indirizzo','Sede - N. civico','Sede - CAP',
        'Sede - Comune/Loc. estera','Sede - Provincia','Sede - Nazione','Stabile org. - Indirizzo','Stabile org. - N. civico','Stabile org. - CAP',
        'Stabile org. - Comune','Stabile org. - Provincia','Stabile org. - Nazione','Rappr. fisc. - Nazione','Rappr. fisc. - Partita IVA','Rappr. fisc. - Denominazione',
        'Rappr. fisc. - Cognome','Rappr. fisc. - Nome','Tipo documento','Numero','Data','Data registrazione','Imponibile/Importo','Aliquota','Imposta','Natura','Esigibilita IVA']

        fcsv = os.getcwd()+'/'+filename+'.csv'
        cv = open(fcsv,'wb')
        wr = csv.writer(cv,quoting=csv.QUOTE_ALL)
        wr.writerow([unicode(entry.replace('"','')).encode('utf-8') for entry in header])

        for riga in data:

            towrite = [riga['tycom'],riga['country'],riga['vat'],riga['cf'],riga['denom'],riga['cognome'],riga['nome'],riga['indirizzo'],
            riga['civico'],riga['cap'],riga['comune'],riga['provincia'],riga['nazione'],'','','','','','','','','','','',
            riga['tydoc'],riga['numero'],riga['data'],riga['rdata'],riga['imponibile'],riga['aliquota'],riga['imposta'],riga['natura'],riga['esigibile']]

            wr.writerow([unicode(entry).encode('utf-8') for entry in towrite])

        cv.close()


print(datetime.strftime(datetime.today(),'%H:%M:%S-%s'))

#spesometro = spesometro('2017-01-17','2017-01-31')
spesometro = spesometro()
spesometro.generaData()
spesometro.genCSV()

print(datetime.strftime(datetime.today(),'%H:%M:%S-%s'))

