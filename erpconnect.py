import datetime, time
import ast
import json
import xmlrpclib
from multiprocessing import Pool
import sys
import os
import urllib, csv
import pprint 

class ErpConnector(object):
    def __init__( self, auth ):

        self.username = auth['username']    # the user
        self.pwd = auth['pwd']    # the password of the user
        self.dbname = auth['dbname']    # the database

        # Get the uid
        sock_common = xmlrpclib.ServerProxy( auth['address_common'] )
        self.uid = sock_common.login( self.dbname, self.username, self.pwd )

        self._sock = xmlrpclib.ServerProxy( auth['address_object'] )

    def search( self, table, args ):
            return self._sock.execute( self.dbname, self.uid, self.pwd, table, 'search', args )

    def read( self, table, id, fields ):
            return self._sock.execute( self.dbname, self.uid, self.pwd, table, 'read', id, fields )

    def create( self, table, args ):
            return self._sock.execute( self.dbname, self.uid, self.pwd, table, 'create', args )

    def write( self, table, id, args ):
            return self._sock.execute( self.dbname, self.uid, self.pwd, table, 'write', id, args )

    def execute( self, table, method, id, args ):
            return self._sock.execute( self.dbname, self.uid, self.pwd, table, method, id, args )